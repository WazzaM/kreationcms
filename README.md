# KreationCms

## Description

A static site generator built with the following agenda:
- simple configuration
- strong convention for themes
- ability to run as cloud serverless solution (AWS Lambda).

Was originally written for the domain kreation.space and contributed
by Warwick Molloy, Lokel Digital PTY LTD.

Other contributors are welcome as long as they agree to use the same open source license.

## License and Contributions

This uses the Apache 2 license and is Open Source.
Contributions are welcome in any form:
- documentation
- issues
- pull-requests (from a fork)
- themes.

A certain number of themes should, eventually, be included
in the main project with selection done via the (JSON) configuration
file.

Themes for KreationCms can be nominated by raising an issue and
the ones that comply with the standards will be linked
from this README for simplicity.
