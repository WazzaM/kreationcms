/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

let Log = require('./log');
let Config = require('./config_subsystem');


//---=== SiteOrganisation Class ===---

/**
 * Defines a policy for putting certain file layouts
 * in certain directories when generating content.
 */
const Mapping = {
  'page': { base: '', depth: 0 },
  'styles': { base: 'styles/', depth: 1},
  'assets': { base: 'assets/', depth: 1},

  find: function(layout) {
    if (layout === undefined || typeof(layout) != 'string') {
      throw new Error(`Expected layout string but got ${layout}`);
    }
    let info = Mapping[layout];
    return (!! info) ? info : { base: `${layout}/`, depth: 1};
  }
};

class SiteOrganisation {
  constructor() {
    this.Fixed = {
      page: 'page',
      styles: 'styles',
      assets: 'assets'
    };
  }

  getResourceDir(basedir, layout) {
    return getPathFor(basedir, layout);
  }

  getFullpath(basedir, layout, name, extension) {
    extension = defaultHtmlExtension(extension);
    return `${getPathFor(basedir, layout)}${name}.${extension}`;
  }

  getLink(fromLayout, toLayout, name, extension) {
    extension = defaultHtmlExtension(extension);
    let relative = determineRelPath(fromLayout, toLayout);
    return `${relative}${name}.${extension}`;
  }
}

// private

function defaultHtmlExtension(extension) {
  return (!! extension) ? extension : 'html';
}

function getPathFor(basedir, layout) {
  return `${basedir}/${Mapping.find(layout).base}`;
}

//---=== SiteOrganisation Class ===---

/*
  Cases

  #page1.html         Depth 0
  #styles/x.css       Depth 1
  #article/foo.html   Depth 1
  #assets/crazy.png   Depth 1

  page -> style       ./styles/x.css    1 - 0 => 1
  article -> style    ../styles/x.css   1 - 1 => 0 but different 'layout'
  article -> page     ../page1.html     0 - 1 => -1
  article -> article  ./art2.html       1 - 1 => 0 same layout
 */

function determineRelPath(fromLayout, toLayout) {
  let relpath;

  Log.debug(`From ${fromLayout}  To ${toLayout}`);
  if (isSameLayerGroup(fromLayout, toLayout)) {
    relpath = './';
  } else {
    let fromDesc = Mapping.find(fromLayout);
    let toDesc = Mapping.find(toLayout);
    let basedir = relBaseForDepth(fromDesc.depth);
    let subdir = toDesc.base;
    relpath = `${basedir}${subdir}`;
    Log.debug(`Rel path: to subdir ${subdir}`);
  }
  Log.debug(`rel path= ${relpath}`);
  return relpath;
}

function isSameLayerGroup(fromLayout, toLayout) {
  return fromLayout === toLayout;
}

function relBaseForDepth(depth) {
  let relbase;

  if (depth === 0) {
    relbase = './';
  } else {
    relbase = '';
    while(depth > 0) {
      relbase = (relbase.length > 0) ? `${relbase}/..` : '../';
      depth--;
    }
  }
  Log.debug(`Relative Base: ${relbase}`);
  return relbase;
}


module.exports = SiteOrganisation;
