/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const Log = require('./log_subsystem');
const LocalStorage = require('./local-disk');

const CONFIG = `sg-config.json`;
const STAGE1_OUTPUT = './temp';

/*
 *  --== Config Format Specification as Lambdas ==--
 */

const ConfiguredLogOptions = (cfg) => cfg.log_options;
const ConfiguredTemplateOptions = (cfg) => cfg.template_options;
const ConfiguredSiteConfig = (cfg) => cfg.site;
const ConfiguredSiteRoutes = (cfg) => cfg.site.route;
const ConfiguredSiteDomain = (cfg) => ConfiguredSiteConfig(cfg).domain;
const ConfiguredThemeName = (cfg) => cfg.theme;
/*
 *  --== Config Format Specification as Lambdas ==--
 */


let defaultLogOptions = Log.default_options;
let __ConfigReader = new LocalStorage();

let ContentReader;
let OutputWriter;

function setAwsContentSource(contentBucket) {}
function setAwsSiteTarget(siteBucket) {}

function setLocalContentSource(base) {
  ContentReader = new LocalStorage(base);
}

function setLocalSiteTarget(base) {
  OutputWriter = new LocalStorage(base);
}

function getContentSource() {
  return ContentReader;
}

function getSiteTarget() {
  return OutputWriter;
}

function getConfigAndThemeReader() {
  return __ConfigReader;
}

var config = undefined;

function processConfig(fname, logOptions) {
  let data = __ConfigReader.readFile(fname); //fs.readFileSync(fname, {encoding: 'utf8'});
  readJsonAndThen(logOptions, data, jsObject => config = jsObject);
}

function readJsonAndThen(logOptions, json, callback) {
  let jsObject = JSON.parse(json);
  if (!jsObject) {
    Log.log_error(logOptions, "Could not parse JSON file.");
  }
  callback(jsObject);
}

// Force Reload of config data
function reloadConfig(options = defaultLogOptions) {
  processConfig(CONFIG, options);
  return config;
}

function ensureLogOptionsPresent() {
  if(! ConfiguredLogOptions(config) ) {
    config.LogOptions = Log.default_options;
  }
}

function getConfig(defunct_options ) {
  if (defunct_options) {
    throw new Error("getConfig() should not have log options.");
  }

  if (! config) {
    return reloadConfig(Log.default_options);
  } else {
    ensureLogOptionsPresent();
    return config;
  }
}

function getLogOptions() {
  var cfg = getConfig();
  return ConfiguredLogOptions(cfg);
}

//  Configured EJS Template Options
//  See - https://ejs.co/#docs
function getTemplateOptions() {
  let cfg = getConfig();
  let templatOptions = ConfiguredTemplateOptions(cfg);
  return (!! templatOptions) ? templatOptions : {};
}

function getSiteDescription() {
  let cfg = getConfig();
  let site = ConfiguredSiteDomain(cfg);
  return (!! site) ? site : '';
}

function getSiteConfig() {
  let cfg = getConfig();
  let site = ConfiguredSiteConfig(cfg);

  return (!!site) ? site: {};
}

function getSiteRoutes() {
  let cfg = getConfig();
  let routes = ConfiguredSiteRoutes();
  if (!! routes) {
    if (! routes.index) {
      routes.index = '';
    }
    if (! routes.error) {
      routes.error = '';
    }
  } else {
    routes = {
      index: '',
      error: ''
    };
  }
  return routes;
}

function getSiteIndexContent() {
  return getSiteRoutes().index;
}

function getSiteErrorContent() {
  return getSiteRoutes().error;
}

function getLocalOutputDir() {
  return STAGE1_OUTPUT;
}

function getThemeName() {
  let cfg = getConfig();
  let theme = ConfiguredThemeName(cfg);
  return (!!theme) ? theme : 'default';
}

module.exports = {
  getConfig,
  reloadConfig,
  getLogOptions,
  getSiteConfig,
  getSiteDescription,
  getSiteErrorContent,
  getSiteIndexContent,
  getLocalOutputDir,
  getTemplateOptions,
  getThemeName,
  setLocalContentSource,
  setLocalSiteTarget,
  getContentSource,
  getSiteTarget,
  getConfigAndThemeReader
};
