/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const Log = require('./log');
const YAML = require('yaml');
const Util = require('./utils');

const Config = require('./config_subsystem');

const KEYWORDS = {
  layout: 'layout',
  title: 'title',
  tags: 'tags'
};


const YAML_MARK = '---';


// -- Content
class Content {
  constructor(file_text, path, file) {
    this.Path = path;
    this.Filename = file;
    this.Name = Util.stripExtensionFromFilename(this.Filename);
    [this.FrontMatter, this.Markdown] = extractFrontMatterAndMarkdown(file_text);
    this.Layout = getLayout(this);
  }

  sourceFilePath() {
    return `${this.Path}/${this.Filename}`;
  }

  hasFrontMatter() {
    return !! this.FrontMatter;
  }
}

// - Private Methods
function extractFrontMatterAndMarkdown(file_text) {
  let frontMatter, markdown;

  if (! file_text) {
    frontMatter = noFrontMatter();
    markdown = '';
    Log.warn(`Cannot create Content class because file content was empty!`);
  } else if ( typeof(file_text) === 'string') {
    var parts = file_text.split( YAML_MARK );
    Log.debug(`Split broke text into ${parts.length} pieces`);

    if (splitTextHasFrontMatter(parts)) {
      [frontMatter, markdown] = extract_front_matter_and_body(parts);
    } else {
      [frontMatter, markdown] = no_front_matter_extract_body(file_text);
    }
  }
  return [frontMatter, markdown];
}

function getLayout(content) {
  let frontMatterLayout = content.FrontMatter[KEYWORDS.layout];
  if (! content.hasFrontMatter() || ! frontMatterLayout) {
    let parentDir = Util.getParentDirName(content.Path);
    let split = parentDir.split('/');
    let presumedLayout = (split.length > 0)
      ? split[split.length - 1]
      : '';
    Log.warn(`Content file: ${content.Path}/${content.Filename} does not specify layout in Front Matter. Will use ${presumedLayout} based on directory where markdown was found.`);
    return presumedLayout;
  }
  return frontMatterLayout;
}

function extract_front_matter_and_body(parts) {
  const FRONT_MATTER_INDEX = 1, MARKDOWN_INDEX = 2;

  let frontMatter = YAML.parse( parts[FRONT_MATTER_INDEX] );
  let markdown = parts[MARKDOWN_INDEX];
  return [frontMatter, markdown];
}

function noFrontMatter() {
  return undefined;
}

function no_front_matter_extract_body(file_text) {
  let frontMatter = noFrontMatter();
  let markdown = file_text;
  return [frontMatter, markdown];
}

//-- Content

function splitTextHasFrontMatter(split_text) {
  const GAP = 0, EXPECTED_LENGTH_WITH_FRONT_MATTER = 3;

  return Util.isEmptyString(split_text[GAP])
    && split_text.length === EXPECTED_LENGTH_WITH_FRONT_MATTER;
}

function readContentFormatFile(path, filename) {
  let fullpath = `${path}/${filename}`;

  Log.debug(`Reading: ${fullpath}`);
  let reader = Config.getContentSource();
  let text = reader.readFile(fullpath);
  Log.debug(`Got ${typeof(text)} of ${typeof(text)==='string'? text.length : 0} bytes`);
  return new Content(text, path, filename);
}

//----------------
module.exports = {
  keywords: KEYWORDS,
  Content,
  readContentFormatFile
};
