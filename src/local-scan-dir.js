/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */


const fs = require('fs');

const LogSub = require('./log_subsystem');

const _LogOptions = {
  debug: true,
  warn: true,
  error: true
};


class FileMatch {
  constructor(path, name) {
    this.Path = path;
    this.Name = name;
  }
}

function scanDirRecursive(root_dir, filename_regex) {
  let list = [];
  scanAllDirs(root_dir, filename_regex, list);
  LogSub.log_debug( _LogOptions, `Scan in directory ${root_dir} done`);
  return list;
}

function scanOneDir(_dir, filename_regex) {
  let list = [];
  scanDirectory(_dir, (file)=> matchFile(_dir, file, filename_regex, list));
  return list;
}

function scanAllDirs(root_dir, filename_regex, list) {
  scanDirectory(
    root_dir,
    (file)=> matchFile(root_dir, file, filename_regex, list),
    (_dir) => scanAllDirs(_dir, filename_regex, list)
  );
}


function scanDirectory(_dir, fileCallback, dirCallback) {
  entries = fs.readdirSync(_dir, {withFileTypes: true});
  for(var entry of entries) {
    if ( ! entry) {
      LogSub.log_error(_LogOptions, `Got an undefined directory entry in directory: ${_dir}`);
    } else if (entry.isDirectory()) {
      LogSub.log_debug(_LogOptions, `Dir: ${entry.name}`);
      if (dirCallback) {
        dirCallback(`${_dir}/${entry.name}`);
      }
    } else if (entry.isFile()) {
      LogSub.log_debug(_LogOptions, `File (or other): ${entry.name}`);
      if (fileCallback) {
        fileCallback(entry.name);
      }
    }
  }
}

function isMatch(name, regex) {
  return !! name.match(regex);
}

function matchFile(path, name, regex, list) {
  if (isMatch(name,regex)) {
    LogSub.log_debug(_LogOptions, `Found file: ${name}`);
    list.push(new FileMatch(path, name));
    LogSub.log_debug(_LogOptions, `List now has ${list.length} files`);
  } else {
    LogSub.log_debug( _LogOptions, `Not a match... ${name}`);
  }
}

module.exports = {
  scanDirRecursive,
  scanOneDir
}