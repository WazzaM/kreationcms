/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const Config = require('./config_subsystem');


let theme_dir;
let layouts_dir;
let templates_dir;
let styles_dir;

function initTheme() {
  theme_dir = `./themes/${Config.getThemeName()}`;
  layouts_dir = `${theme_dir}/layouts`;
  templates_dir = `${theme_dir}/templates`;
  styles_dir = `${theme_dir}/style`;
}

function ensureInitialised() {
  if (! theme_dir) {
    initTheme();
  }
}

function getThemeDir() {
  ensureInitialised();
  return theme_dir;
}

function getLayoutsDir() {
  ensureInitialised();
  return layouts_dir;
}

function getTemplatesDir() {
  ensureInitialised();
  return templates_dir;
}

function getStylesDir() {
  ensureInitialised();
  return styles_dir;
}

module.exports = {
  getThemeDir,
  getLayoutsDir,
  getTemplatesDir,
  getStylesDir
};
