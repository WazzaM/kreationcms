/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

let Template = require('./template');
let Log = require('./log');
let SiteOrganisation = require('./site-organisation');
let Content = require('./content');
let Style = require('./style');
let Config = require('./config_subsystem');


/**
 * Generate HTML from content source files once.
 */
function generateHtml() {
  
  function processContent(content) {
    let layoutName = content.Layout;
    let template = Template.getTemplate(layoutName);
    if (template) {
      Log.info(`Rendering ${content.sourceFilePath()}`);
      template.renderTemplate(content);
    } else {
      Log.warn(`Did not find template ${layoutName} for Content: ${content.sourceFilePath()}`);
    }
  }
  
  Config.setLocalContentSource('.');
  Config.setLocalSiteTarget('.');

  Content.forEachContentFile( (content) => processContent(content));

  Style.compileAllStyles();
}


module.exports = {
  generateHtml
};