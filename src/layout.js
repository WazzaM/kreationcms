/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const ContentSubsystem = require('./content_subsystem');
const Log = require('./log');
const Themes = require('./themes');
const Util = require('./utils');
const Config = require('./config_subsystem');



class LayoutInfo {
  constructor(path, filename) {
    this.LayoutPath = path;
    this.LayoutFilename = filename;
    let content = ContentSubsystem.readContentFormatFile(path, filename);

    if (content.hasFrontMatter()) {
      this.ExpectedFrontMatter = content.FrontMatter;
      this.Layout = this.ExpectedFrontMatter[ContentSubsystem.keywords.layout];
      Log.debug(`Layout ${filename} has layout: ${this.Layout}`);
    }
    associateLayout(this, content);
  }

  compliesWithFrontMatter(content) {
    let isCompliant = true;
    
    assertIfLayoutsDontMatch(this, content);

    for(var key in this.ExpectedFrontMatter) {
      let value = content.FrontMatter[key];
      Log.debug(`Content Frontmatter - expected key: ${key}  has key? ${!!value}`);
      isCompliant = isCompliant && (!! value);
    }
    return isCompliant;
  }
}

// -- private methods
function associateLayout(layoutInfo, content) {
  let layoutNameFromFilename = Util.stripExtensionFromFilename(content.Filename);
  let layoutFromDeclaration = getDeclaredLayoutName(layoutInfo.ExpectedFrontMatter);
  if (layoutFromDeclaration != layoutNameFromFilename) {
    Log.warn(`Layout name declared in template is ${layoutFromDeclaration} but ${layoutNameFromFilename} was expected.`);
  }
  layoutInfo.Layout = layoutNameFromFilename;
}

function assertIfLayoutsDontMatch(layoutInfo, content) {
  let layoutFromContent = content.Layout;

  if (layoutFromContent != layoutInfo.Layout) {
    throw new Error(
      `LayoutInfo[Layout=${layoutInfo.Layout}] asked to check Content[path=${content.Path},File=${content.Filename}] with layout: ${layoutFromContent}`
      + "\nThis makes no sense as the layout is not intended for that content file."
    );
  }
}

//------ LayoutInfo Class ----

function getDeclaredLayoutName(expectedFrontMatter) {
  let expectedLayout = expectedFrontMatter[ContentSubsystem.keywords.layout];

  if (! expectedLayout ) {
    Log.warn(`Theme layout file for ${this.Layout} does not specify the layout in the front matter.`);
  }
  return expectedLayout;
}

let LayoutSet = undefined;

function ensureLayoutsInitialised() {
  if (! LayoutSet) {
    LayoutSet = {};
    loadEachLayoutFile();
  }
}

function loadEachLayoutFile() {
  let themeReader = Config.getConfigAndThemeReader();
  let layouts = themeReader.scanDir( Themes.getLayoutsDir(), /.*\.(md)/);
  Log.debug(`Configuring Layouts - found ${layouts.length} layouts`);
  for(let lo of layouts) {
    let _layout = new LayoutInfo(lo.Path, lo.Name);
    LayoutSet[_layout.ExpectedFrontMatter[ContentSubsystem.keywords.layout]] = _layout;
  }
}

function hasLayout(layoutName) {
  ensureLayoutsInitialised();
  return !! LayoutSet[layoutName];
}

function getLayoutInfo(layoutName) {
  ensureLayoutsInitialised();
  return LayoutSet[layoutName];
}

function getListOfLayouts() {
  ensureLayoutsInitialised();
  let list = [];
  for(let layout in LayoutSet) {
    list.push(layout);
  }
  return list;
}

module.exports = {
  LayoutInfo,
  hasLayout,
  getLayoutInfo,
  getListOfLayouts
};
