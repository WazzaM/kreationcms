/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const LogSubsystem = require('./log_subsystem');
const ConfigSubsystem = require('./config_subsystem');

const log_options = ConfigSubsystem.getLogOptions();

function log_debug(message) {
  LogSubsystem.log_debug(log_options, message);
}

function log_info(message) {
  LogSubsystem.log_info(log_options, message);
}

function log_warn(message) {
  LogSubsystem.log_warn(log_options, message);
}

function log_error(message) {
  LogSubsystem.log_error(log_options, message);
}

function forceLocalLogLevel(level) {
  log_options[level] = true;
}

module.exports = {
  debug: log_debug,
  info: log_info,
  warn: log_warn,
  error: log_error,

  forceLocalLogLevel
};
