/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const LogSub = require('./log_subsystem');
const Utils = require('./utils');

const fs = require('fs');
const ScanDir = require('./local-scan-dir');

const _LogOptions = {
  debug: true,
  warn: true,
  error: true
};


/**
 * A pluggable storage object for theme development
 * or local content preview before publishing.
 */
class LocalStorage {
  constructor(base) {
    base = (!!base && typeof(base) === 'string') ? base : '.';
    this.BaseDir = base;
  }

  readFile(fullpath) {
    return readTextFile(fullpath);
  }

  writeFile(fullpath, data) {
    writeTextFile(fullpath, data);
  }

  isDirectory(fullpath) {
    return isDirectory(fullpath);
  }

  isFile(fullpath) {
    return isFile(fullpath);
  }

  makeDirectory(fullpath, isRecursive) {
    makeDir(fullpath, isRecursive);
  }

  scanDir(rootDir, filename_regex) {
    return ScanDir.scanDirRecursive(rootDir, filename_regex);
  }

  scanOneDir(_dir, filename_regex) {
    return ScanDir.scanOneDir(_dir, filename_regex);
  }

  toString() {
    return "LocalStorage()";
  }
}

// -- implementations --

function readTextFile(fullpath) {
  try {
    let data = fs.readFileSync(fullpath, {encoding: 'utf8'});
    return data;
  } catch(e) {
    LogSub.log_error(_LogOptions, `Unable to read text file ${fullpath} - ${e}`);
  }
  return undefined;
}

function writeTextFile(fullpath, data) {
  try {
    ensurePathExists(Utils.getParentDirName(fullpath) );
    fs.writeFileSync(fullpath, data, {encoding: 'utf8'});
  } catch(e) {
    LogSub.log_error(_LogOptions, `Unable to write file ${fullpath} due to ${e}`);
  }
}

function ensurePathExists(fullpath) {
  if (Utils.isString(fullpath) && fullpath.length > 0) {
    if (isExistentDirEntry(fullpath)) {
      return;
    }
    if (isExistentDirEntry(fullpath)) {
      LogSub.log_debug(_LogOptions, `Directory already OK: ${fullpath}`);
    } else {
      LogSub.log_debug(_LogOptions, `Creating directory: ${fullpath}`);
      makeDir(fullpath, true);
    }
  }
}

function copyFile(sourceFile, destPath) {
  if (isExistentDirEntry(destPath)) {
    fs.copyFileSync(sourceFile, destPath);
  } else {
    throw new Error(`Directory ${destPath} does not exist`);
  }
}

function makeDir(fullpath, isRecursive) {
  let recursion = (!! isRecursive);
  try {
    if (! recursion) { 
      fs.mkdirSync(fullpath);
    } else {
      recursivelyMakeDir(fullpath);
    }
  } catch(e) {
    LogSub.log_error(_LogOptions, `Unable to make directory ${fullpath} due to ${e}`);
  }
}

function recursivelyMakeDir(path) {
  if (isExistentDirEntry(path)) {
    return;
  } else {
    let parent = Utils.getParentDirName(path);
    recursivelyMakeDir(parent);
    fs.mkdirSync(path);
  }
}

function isExistentDirEntry(fullpath) {
  return fs.existsSync(fullpath);
}

function isDirectory(fullpath) {
  let stat = fs.statSync(fullpath);
  return stat.isDirectory();
}

function isFile(fullpath) {
  LogSub.log_debug(_LogOptions, `isFile() path: ${fullpath}`);
  let stat = fs.statSync(fullpath);
  return stat.isFile();
}

module.exports = LocalStorage;
