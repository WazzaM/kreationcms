/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const posthtml = require('posthtml');

const Config = require('./config_subsystem');
const Themes = require('./themes');
const Layouts = require('./layout');
const Util = require('./utils');
const Log = require('./log');
const ContentData = require('./content_data');
const SiteOrganisation = require('./site-organisation');

let Site = new SiteOrganisation();

let ejs = require('ejs');

function resolveInclude(name, filename, isDir) {
  Log.debug(`EJS: resolveInclude name= ${name}, filename= ${filename}, isDir= ${isDir} `);
  let fullpath = `${filename}/${name}.ejs`;
  let reader = Config.getConfigAndThemeReader();

  if (reader.isFile(fullpath)) {
    Log.debug(`Confirmed file: ${fullpath}`);
  } else {
    Log.warn(`Not a file - problem with path? ${fullpath}`);
  }
  return fullpath;
}

ejs.resolveInclude = resolveInclude;


function loadPartial(relativePath) {
  let reader = Config.getContentSource();
  Log.debug(`EJS loading file from: ${relativePath} using reader: ${reader}`);

  return reader.readFile(relativePath);
}

ejs.fileLoader = loadPartial;


// ---------- Template Class ---------
class Template {
  constructor(path, filename) {
    this.Path = path;
    this.Filename = filename;
    this.Layout = Util.stripExtensionFromFilename(this.Filename);
    readTemplateFile(this);
  }

  isReady() {
    return (!! this.compiledTemplate);
  }

  sourceFilePath() {
    return `${this.Path}/${this.Filename}`;
  }

  renderTemplate(content) {
    if (this.isReady()) {
      let context = new ContentData(content);
      let html = this.compiledTemplate(context);
      let path = Site.getFullpath(Config.getLocalOutputDir(), content.Layout, content.Name);
      Log.debug(`Determined template path ${path} for Content ${content.Filename}`);
      processMarkdown(path, html);
    } else {
      Log.error(`Template ${this.sourceFilePath()} was not ready - `)
    }
  }
}

// privates

function readTemplateFile(template) {
  let fqFile = template.sourceFilePath();
  let reader = Config.getContentSource();
  let data = reader.readFile(fqFile);
  if (data) {
    template.compiledTemplate = ejs.compile( data,  getEjsOptions() );
    Log.debug(`Compiled template for layout ${template.Layout}`);
  } else {
    Log.error(`Unable to load template source from: ${fqFile}`);
  }
}


function processMarkdown(path, htmlIn) {
  let options = {};
  let writer = Config.getSiteTarget();

  posthtml([
    require('posthtml-md')(options)
  ])
  .process(htmlIn, /* options */)
  .then( htmlOut => writer.writeFile(path, htmlOut.html) );
}
// ---------- Template Class ---------

let ejsOptions = undefined;

function getEjsOptions() {
  ejs.fileLoader = loadPartial;
  if (! ejsOptions) {
    let FixedOptions = {
      filename: Themes.getTemplatesDir()
    };
    let configuredOptions = Config.getTemplateOptions();
    ejsOptions = Util.mergeObjects(configuredOptions, FixedOptions);
  }
  return ejsOptions;
}

function createTemplateObject(path, filename) {
  let fqFile = `${path}/${filename}`;
  if (hasMatchingLayout(filename)) {
    let templateObject = new Template(path, filename);
    insertIntoSet(templateObject);
  } else {
    Log.warn(`Template file ${fqFile} has no corresponding layout defined in that theme! SKIPPED.`);
  }
}

function hasMatchingLayout(filename) {
  let layoutName = Util.stripExtensionFromFilename(filename);
  let layoutInfo = Layouts.getLayoutInfo(layoutName);
  if (! layoutInfo) {
    Log.warn(`Template ${filename} doesn't have associated Layout ${layoutName}`);
  }
  return (!! layoutInfo);
}

function insertIntoSet(template) {
  TemplateSet[template.Layout] = template;
}

let TemplateSet = undefined;

function ensureTemplateSetInitialised() {
  if (! TemplateSet) {
    TemplateSet = {};
    findAllTemplates();
  }
}

function findAllTemplates() {
  let themeReader = Config.getConfigAndThemeReader();
  let templates = themeReader.scanOneDir(Themes.getTemplatesDir(), /.*\.(ejs)/ );
  for(let template of templates) {
    createTemplateObject(template.Path, template.Name);
  }
}

function getTemplate(layoutName) {
  ensureTemplateSetInitialised();
  let template = TemplateSet[layoutName];
  if (! template) {
    Log.warn(`No template found for layout: ${layoutName}`);
    return undefined;
  }
  return template;
}

module.exports = {
  getTemplate,
  initialiseTemplates: ensureTemplateSetInitialised
};
