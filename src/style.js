/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

let Log = require('./log');
let Util = require('./utils');
let Themes = require('./themes');
let SiteOrganisation = require('./site-organisation');
let Config = require('./config_subsystem');

let Sass = require('node-sass');

let Site = new SiteOrganisation();


function compileAllStyles() {
  let styleList = findStyles();
  for(fileMatch of styleList) {
    let fullpath= `${fileMatch.Path}/${fileMatch.Name}`;
    let style_content = readStyle(fullpath);
    processStyle(style_content, fileMatch.Path, fileMatch.Name);
  }
}

function findStyles() {
  let themeReader = Config.getConfigAndThemeReader();
  let list = [];
  list = themeReader.scanDir(Themes.getThemeDir(), /^[^\_].*\.(scss)$/);
  Log.debug(`Found ${list.length} style files.`);
  return list;
}

function readStyle(fullpath) {
  Log.debug(`Reading file: ${fullpath}`);
  let reader = Config.getContentSource();
  let style_content = reader.readFile(fullpath);
  return style_content;
}

function processStyle(style_content, sourcePath, sourceScss) {
  let cssName = cssIfyName(sourceScss);
  let scssFullpath = `${sourcePath}/${sourceScss}`;

  let result = Sass.renderSync({
    data: style_content,
    file: sourceScss,
    outFile: cssName,
    importer
  });
  let writer = Config.getSiteTarget();
  writer.writeFile(cssName, result.css);
}

function cssIfyName(name) {
  let base = Util.stripExtensionFromFilename(name);
  return Site.getFullpath(Config.getLocalOutputDir(), Site.Fixed.styles, base, 'css');
}

function importer(scssUrl, importingScssName) {
  Log.debug(`File ${importingScssName} importing ${scssUrl}`);
  let importPath = `${Themes.getStylesDir()}/_${scssUrl}.scss`;
  let reader = Config.getContentSource();
  return {
    contents: reader.readFile(importPath)
  };
}

module.exports = {
  compileAllStyles
};
