/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const Content = require('./content');
const Config = require('./config_subsystem');
const SiteOrganisation = require('./site-organisation');

// ---=== ContentData Class ===---
//
//  This is passed to each template as the data variable
//  so this is the reference for template authors.
//
class ContentData {
  constructor(contentObj) {
    this.FrontMatter = contentObj.FrontMatter;
    this.Markdown = contentObj.Markdown;
    this.Layout = contentObj.Layout;
    this.Site = Config.getSiteDescription();
    this.LayoutList = Content.getListOfContentLayouts();
    this.LayoutContentMap = createLayoutContentMap();
    this.site = new SiteOrganisation();
  }
}

// private

function createLayoutContentMap() {
  let map = {};
  for (let layout of Content.getListOfContentLayouts() ) {
    map[layout] = Content.getContentByLayout(layout);
  }
  return map;
}

// ---=== ContentData Class ===---

module.exports = ContentData;
