/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const defaultLogOptions = {
  debug: false,
  info:false,
  warn: true,
  error: true
};

const debugLogOptions = {
  debug: false,
  info: true,
  warn: true,
  error: true
};

function log_debug(options, message) {
  if (options && options.debug) {
    console.log(`DEBUG: ${message}`);
  }
}

function log_info(options, message) {
  if (options && options.info) {
    console.log(`INFO: ${message}`);
  }
}

function log_warn(options, message) {
  if (options.warn) {
    console.warn(`WARN: ${message}`);
  }
}

function log_error(options, message) {
  if (options.error) {
    console.error(`ERROR: ${message}`);
  }
}

function clean_log_options(options) {
  console.log(`Options In = ${JSON.stringify(options)}`);
  let out = (!! options) ? options : defaultLogOptions;
  console.log(`Options Out = ${JSON.stringify(out)}`);
  return out;
}

module.exports = {
  debug_options: debugLogOptions,
  default_options: defaultLogOptions,

//  options: clean_log_options,
  log_debug,
  log_info,
  log_warn,
  log_error
};
