/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const Log = require('./log');
const ContentSubsystem = require('./content_subsystem');

const Util = require('./utils');
const Layouts = require('./layout');
const Config = require('./config_subsystem');

const CONTENT_DIR = './content';


function loadContentFile(path, filename) {
  let content = ContentSubsystem.readContentFormatFile(path, filename);
  if (content.hasFrontMatter()) {
    validateFrontMatterAndLogIssues(content);
    return content;
  } else {
    Log.warn(`Content file: ${path}/${filename} does not have front matter. Will probably affect layout.`);
    return content;
  }
  return undefined;
}

function validateFrontMatterAndLogIssues(content) {
  let layout_type = content.FrontMatter[ContentSubsystem.keywords.layout];
  Log.debug(`For content: layout_type= ${layout_type}. Exists: ${Layouts.hasLayout(layout_type)}`);

  if (Layouts.hasLayout(layout_type)) {
    let layoutInfo = Layouts.getLayoutInfo(layout_type);
    if ( layoutInfo.compliesWithFrontMatter(content)) {
      Log.info(`Content file: ${content.Path}/${content.Filename} meets layout file spec.`);
    } else {
      Log.warn(`Content file: ${content.Path}/${content.Filename} lacks required a value based on layout ${layout_type}. May cause site generation errors.`);
    }
  } else {
    Log.error(`Content file: ${content.Path}/${content.Filename} specified layout ${layout_type} that is not in the template.`);
  }
}

let ContentSet = undefined;

function ensureContentSetLoaded() {
  if (! ContentSet) {
    ContentSet = {};
    loadAllContentFiles();
  }
}

function loadAllContentFiles() {
  Log.info("loadAllContentFiles()");

  let reader = Config.getContentSource();
  let content_files = reader.scanDir( CONTENT_DIR, /.*\.(md)/ );
  for(let c of content_files) {
    let contentObject = loadContentFile(c.Path, c.Name);
    addContentToLayoutList(contentObject);
  }
}

function addContentToLayoutList(content) {
  if (content.hasFrontMatter()) {
    let layoutName = content.Layout;
    let layoutList = getContentSetLayoutList(layoutName);
    layoutList.push(content);
  } else {
    Log.warn(`Skipping content: ${content.sourceFilePath()}`);
  }
}

function getContentSetLayoutList(layoutName) {
  ensureContentSetLoaded();
  return Util.getListFromHashByKey(ContentSet, layoutName);
}

function getListOfContentLayouts() {
  ensureContentSetLoaded();
  return Util.getKeyListFromHash(ContentSet);
}

function getContentByLayout(layoutName) {
  ensureContentSetLoaded();
  return Util.getReadonlyListFromHashByKey(ContentSet, layoutName);
}

function forEachContentFile(callback) {
  if (!callback) {
    Log.error(`forEachContentFile: called without callback function.`);
    return;
  }
  ensureContentSetLoaded();
  let layouts = getListOfContentLayouts();
  for(let _layout of layouts) {
    let content_list = getContentByLayout(_layout);
    for(let content of content_list) {
      callback(content);
    }
  }
}

module.exports = {
  forEachContentFile,
  getContentByLayout,
  getListOfContentLayouts,
  loadContentFile
};
