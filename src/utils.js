/*  -----
(c) Copyright 2019 Warwick Molloy, Lokel Digital PTY LTD

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
---- */

const LogSub = require('./log_subsystem');

const _LogOptions = {
  debug: true,
  warn: true,
  error: true
};


function isString (x) {
  return  typeof(x) === 'string';
}

function isEmptyString(x) {
  return isString(x) && x.length === 0;
}

// Removes the extension, leaving the name
function stripExtensionFromFilename(filename) {
  let parts = filename.split('.');
  return parts[0];
}


function getParentDirName(fullpath) {
  let nameParts = fullpath.split('/');
  let numParts = nameParts.length;
  if (numParts > 0) {
    return nameParts.splice(0, (numParts - 1)).join('/');
  }
  return '';
}

function getKeyListFromHash(hash) {
  let list = [];
  for(let entry in hash) {
    list.push(entry);
  }
  return list;
}

function getListFromHashByKey(hash, key) {
  let list = hash[key];
  if (! list) {
    list = [];
    hash[key] = list;
  }
  return list;
}

function getReadonlyListFromHashByKey(hash, key) {
  let list = hash[key];
  if (! list) {
    return [];
  }
  return [].concat(list); // Readonly list
}

function mergeObjects(obj1, obj2) {
  let result = {};
  for(let keys1 in obj1) {
    result[keys1] = obj1[keys1];
  }
  for(let keys2 in obj2) {
    result[keys2] = obj2[keys2];
  }
  return result;
}

module.exports = {
  getParentDirName,
  getKeyListFromHash,
  getListFromHashByKey,
  getReadonlyListFromHashByKey,
  isEmptyString,
  isString,
  mergeObjects,
  stripExtensionFromFilename
};

